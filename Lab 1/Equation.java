//Kieu Trong Bang - 20176697

import java.util.*;
import java.lang.Math;
import javax.swing.JOptionPane;
public class Equation{
	public static void main(String[] args){
		System.out.println("First degree equation with one variable:");
		String s;
		s = JOptionPane.showInputDialog(null,"Input first parameter: ","First degree eqn"  ,JOptionPane.INFORMATION_MESSAGE);
		double x = Double.parseDouble(s);
		s = JOptionPane.showInputDialog(null,"Input second parameter: " ,"First degree eqn",JOptionPane.INFORMATION_MESSAGE);
		double y = Double.parseDouble(s);
		double root = -y/x;
		JOptionPane.showMessageDialog(null, root ,"First degree equation root: " ,JOptionPane.INFORMATION_MESSAGE);
		System.out.printf("First degree equation root: %f\n", root);


		System.out.println("First degree equation with 2 variables:");

		s = JOptionPane.showInputDialog(null,"Input param x1 : ","First degree eqn"  ,JOptionPane.INFORMATION_MESSAGE);
		double x1 = Double.parseDouble(s);
		s = JOptionPane.showInputDialog(null,"Input param y1: ","First degree eqn"  ,JOptionPane.INFORMATION_MESSAGE);
		double y1 = Double.parseDouble(s);
		s = JOptionPane.showInputDialog(null,"Input param z1: ","First degree eqn"  ,JOptionPane.INFORMATION_MESSAGE);
		double z1 = Double.parseDouble(s);
		s = JOptionPane.showInputDialog(null,"Input param x2: ","First degree eqn"  ,JOptionPane.INFORMATION_MESSAGE);
		double x2 = Double.parseDouble(s);
		s = JOptionPane.showInputDialog(null,"Input param y2: ","First degree eqn"  ,JOptionPane.INFORMATION_MESSAGE);
		double y2 = Double.parseDouble(s);
		s = JOptionPane.showInputDialog(null,"Input param z2: ","First degree eqn"  ,JOptionPane.INFORMATION_MESSAGE);
		double z2 = Double.parseDouble(s);
		double root_y = (z2*x1 - z1*x2)/(y2*x1-y1*x2);
		double root_x = (z1 - y1*root_y)/x1;
		JOptionPane.showMessageDialog(null, root_x + " and " + root_y,"First degree eqn root x and y : " ,JOptionPane.INFORMATION_MESSAGE);
		System.out.printf("Equation: %fx + %f.2y = %f\n", new Object[]{x1, y1, z1});
		System.out.printf("Equation: %fx + %f.2y = %f\n", new Object[]{x2, y2, z2});
		System.out.printf("Root of equation: %f and %f\n", new Object[]{root_x, root_y});

		System.out.println("Second degree with one variable:");

		s = JOptionPane.showInputDialog(null,"Input param a: ","Second degree eqn"  ,JOptionPane.INFORMATION_MESSAGE);
		double a = Double.parseDouble(s);
		s = JOptionPane.showInputDialog(null,"Input param b: ","Second degree eqn"  ,JOptionPane.INFORMATION_MESSAGE);
		double b = Double.parseDouble(s);
		s = JOptionPane.showInputDialog(null,"Input param c: ","Second degree eqn"  ,JOptionPane.INFORMATION_MESSAGE);
		double c = Double.parseDouble(s);
		double delta = b*b - 4*a*c;
		if (delta < 0){
			JOptionPane.showMessageDialog(null, "The equation has no root","Second degree eqn root: " ,JOptionPane.INFORMATION_MESSAGE);
			System.out.printf("The equation has no root.");
		}
		else if (delta == 0){
			double only_root = -b/(2*a);
			JOptionPane.showMessageDialog(null, only_root,"Second degree eqn root: " ,JOptionPane.INFORMATION_MESSAGE);
			System.out.printf("The equation has double root: %f", only_root);
		}
		else if (delta > 0){
			double root_1 = (-b + Math.sqrt(delta))/(2*a);
			double root_2 = (-b - Math.sqrt(delta))/(2*a);
			JOptionPane.showMessageDialog(null, root_1 + " and " + root_2,"Second degree eqn root: " ,JOptionPane.INFORMATION_MESSAGE);
			System.out.printf("Two distinct root: %f and %f", new Object[]{root_1, root_2});
		}
	}
}